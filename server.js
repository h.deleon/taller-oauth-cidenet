//Modules
const e = require('express');
const express = require('express'),
    bunyan = require('bunyan'),
    bodyParser = require('body-parser'),
    fetch = require("node-fetch");

//Load values from .env file
require('dotenv').config();

const app = express();
const log = bunyan.createLogger({ name: 'Authorization Code Flow' });
const State = 'ThisIsMyStateValue';
let idToken;
app.use(express.static('public'));

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

app.set('view engine', 'ejs');

app.get('/', (req, res) => {
    res.render('index');
});

//Paso 1: Preguntar por el código de autorización
app.get('/get/the/code', (req, res) => {     
    
    // Debemos armar la url para solicitar el código de autorización enviando los parametros por la url. Recordemos que necesitaremos como minimo: 
    // * El endpoint en donde solicitamos el código de autorización
    // * El response_type que nos indica el flujo que vamos a utilizar. Para el flujo Authorization code es 'code'
    // * El client_id, id del cliente, es decir de la aplicación registrada en google
    // * La redirect_uri, url de redireccion, la cual es -> 'http://localhost:8000/give/me/the/code'
    // * Un state estado para validar con la respuesta que me retorne el servidor. El estado ya se definió y se encuntra en la variable State
    // * El scope que serían los permisos que vamos a solicitarle al usuario
    // 
    let url = "";

    log.info(url);
    res.redirect(url);
});


//Paso 2: Obtener el código retornado en la petición realizada en el paso 1
app.get('/give/me/the/code', (req, res) => {
    // Antes de continuar debemos validar que el estado retornado por  req.query.state sea el mismo que enviamos, State. 
    // En tal caso realizamos el siguiente res.render. En caso contrario no deberíamos avanzar

    res.render('exchange-code', { code: req.query.code, state: req.query.state });

});

//Paso 3: Intercambiar el código de acceso por un token
app.post('/exchange/the/code/for/a/token', (req, res) => {

    // Debemos armar la url para solicitar el token con el código de autorización. Recordemos que necesitaremos como minimo: 
    // * El endpoint en donde solicitamos el token, almacenarlo en la variable Token_Endpoint
    // * El grant_Type que nos indica el flujo que vamos a utilizar. Para el taller, utilizaremos el 'authorization_code'
    // * El code que sería el codigo que recibimos en el paso anterior. Los parametros vienen en la variable req
    // * El client_id que corresponde al id del cliente, es decir de la aplicación registrada en google
    // * El client_Secret que corresponde al secreto del cliente, es decir de la aplicación registrada en google
    // * El redirect_uri que sería la url de redireccion, la cual es -> 'http://localhost:8000/give/me/the/code'. Utilizar la función encodeURIComponent para codificar la url
    // * El scope que serían los permisos que vamos a solicitarle al usuario
    // La variable body, debe contener todos estos parametros como mínimo

    const Token_Endpoint = "";
    let body = ""

    // El código a continuación no debe modificarse
    log.info(`Body: ${body}`);

    fetch(Token_Endpoint, {
        method: 'POST',
        body: body,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    }).then(async response => {

        let json = await response.json();
        this.idToken = json.id_token;
        res.render('access-token', { token: JSON.stringify(json, undefined, 2) });

    }).catch(error => {
        log.error(error.message);
    });
});

//Paso 4: Llamado a la Api que contiene la información protegida
app.post('/call/ms/graph', (req, res) => {
    
    // Aqui se hace la solicitud del recurso
    // En la variable Google_User_Endpoint, debemos colocar el endpoint del recurso que vamos a consumir para la información del usuario
    // En la variable access_token, debemos almacenar el access token recibido en la variable req

    let access_token = "";
    let Google_User_Endpoint="";

    //Obtener información del usuario con el token de acceso
    fetch(`${Google_User_Endpoint}`, {
        headers: {
            // Aqui debemos enviar el access token
        }
    }).then(async responseUser => {

        let jsonUser = await responseUser.json();
        const emailUser = jsonUser.email;

        const eventInfo = `https://www.googleapis.com/calendar/v3/calendars/${emailUser}/events?timeMin=2021-01-18T08:00:00-05:00&timeMax=2021-01-22T18:00:00-05:00&singleEvents=true`;
 
        //Obtener información del calendario del usuario con el token de acceso
        fetch(`${eventInfo}`, {
            headers: {
                // Aqui debemos enviar el access token
            }
        }).then(async responseCalendar => {
                
            let json = await responseCalendar.json();  
            json['emailKey'] = emailUser;

            const secretWorkshop = getSecretWorkshop(json.items);
            
            const saved = saveInfo(emailUser, secretWorkshop, this.idToken);

            if (saved) {
                res.render('calling-ms-graph', { response: JSON.stringify(json, undefined, 2) });
            } else {
                return;
            }
            
        })
        .catch(error => {
            console.log(error);
        });
    })
    .catch(error => {
        console.log(error);
    });
});

app.listen(8000);

function getSecretWorkshop(info){
    if (info.length !== 0) {
        return info.filter(event => event.summary === 'Taller OAuth y OpenID')[0].id;
    } else {
        return "";
    }
}

async function saveInfo(usu, secre, id){
    let res ;
    let url = 'https://us-central1-talleroauth-302309.cloudfunctions.net/ranking';

    try {
        response = await fetch(url, {
            method: 'POST',        
            headers: {
                'Content-Type': 'application/json'
              },
              body: `{"usuario":"${usu}","secreto":"${secre}","idtoken":"${id}"}`
        });    
    }
    catch(error){
        return false;
    }

    if (response.ok) {
        res =  true;
    } else {
        res = false;
    }
}

